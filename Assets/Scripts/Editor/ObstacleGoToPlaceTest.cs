﻿namespace theplant
{
    using NSubstitute;

    using UnityEngine;
    using UnityEditor;
    using NUnit.Framework;

    public class ObstacleGoToPlaceTest
    {
        /// <summary>
        /// Tests Unity lerp method.
        /// </summary>
        [Test]
        public void LerpTest()
        {
            var obstacleTransform = new Vector3(0, 0, 0);
            var destination = new Vector3(1f, 1f, 1f);

            obstacleTransform = Vector3.Lerp(obstacleTransform, destination, 0.5f);

            Assert.AreEqual(new Vector3(0.5f, 0.5f, 0.5f), obstacleTransform);
        }

        [Test]
        public void GoToPositionTest()
        {

            var obstacleGoToPlace = new ObstacleGoToPlace();
            obstacleGoToPlace.Destination = new Vector3(1f, 1f, 1f);


            var result = obstacleGoToPlace.MoveToDestination(new Vector3(0, 0, 0));

            Assert
                .AreEqual(new Vector3(0.5f, 0.5f, 0.5f), result);

        }

    }
}