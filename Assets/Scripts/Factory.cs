﻿using UnityEngine;
using System.Collections;

public class Factory : MonoBehaviour
{

    public GameObject SmokeFactory;
    public GameObject CleanFactory;

    public bool isClean;

    void Start()
    {
        this.isClean = false;
    }

    public void Clean()
    {
        this.SmokeFactory.SetActive(false);
        this.CleanFactory.SetActive(true);
    }
}
