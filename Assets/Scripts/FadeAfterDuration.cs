﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Renderer))]
public class FadeAfterDuration : MonoBehaviour
{
    [SerializeField]
    private float speed;

    private Color color;

    private Renderer renderer;

    /// <summary>
    /// Used initialization.
    /// </summary>
    private void Start()
    {
        this.renderer = this.GetComponent<Renderer>();
        this.color = this.renderer.material.color;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Action()
    {
        StartCoroutine(ChangeAlphaToZero());
    }

    IEnumerator ChangeAlphaToZero()
    {
        while (this.color.a != 0)
        {
            this.color.a -= this.speed * Time.deltaTime;
            if (this.color.a < 0)
            {
                this.color.a = 0;
            }
            this.renderer.material.color = this.color;
            yield return null;
        }
    }
}
