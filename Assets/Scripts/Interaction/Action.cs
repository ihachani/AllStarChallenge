﻿namespace theplant
{
    using System;

    using UnityEngine;
    using UnityEngine.UI;

    public class Action : MonoBehaviour
    {

        public int HandleLayer = 8;
        public KeyCode attackKey = KeyCode.E;
        public int distanceToObstacle = 2;
        public String ActionTextName = "ActionText";

        void Start()
        {

        }

        void Update()
        {
            RaycastHit hit;
            Debug.DrawRay(transform.position, this.transform.forward);
            if (Physics.Raycast(transform.position, this.transform.forward, out hit, distanceToObstacle))
            {
                GameObject target = hit.collider.gameObject;
                if (target.layer == HandleLayer)
                {
                    if (Input.GetKeyUp(attackKey))
                    {
                        (target.GetComponent<Handle>()).handle(hit.point);
                    }
                    else
                    {
                        GameObject.Find(ActionTextName).GetComponent<Text>().text =
                            (target.GetComponent<Handle>()).Message;
                    }
                }
                else
                {
                    GameObject.Find(ActionTextName).GetComponent<Text>().text ="";

                }
            }

        }
    }

}