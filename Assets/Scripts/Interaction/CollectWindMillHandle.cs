﻿namespace theplant
{
    using System;

    using UnityEngine;
    using System.Collections;

    public class CollectWindMillHandle : Handle
    {

        public WindMillMission mission;

        void Start()
        {
        }
        public override void handle(Vector3 point)
        {
            base.handle(point);
            this.TakePart();
            this.HideMe();
        }

        void TakePart()
        {
            this.mission.CollectNewPart();
        }

        void HideMe()
        {
            GameObject.Destroy(gameObject);
        }
    }
}