﻿namespace theplant
{
    using UnityEngine;
    using System.Collections;

    public class RemoveHandle : Handle
    {
        public override void handle(Vector3 point)
        {
            base.handle(point);
            this.HideMe();
        }
        void HideMe()
        {
            GameObject.Destroy(gameObject);
        }
    }

}