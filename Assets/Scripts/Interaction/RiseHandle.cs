﻿namespace theplant
{
    using System;
    using UnityEngine;
    using System.Collections;

    public class RiseHandle : Handle
    {

        public Rise Rise;

        public override void handle(Vector3 point)
        {
            base.handle(point);
            this.IntroduceWillMillAt(point);
        }

        void IntroduceWillMillAt(Vector3 point)
        {
            this.Rise.WindMillAt(point);
        }
    }

}