﻿namespace theplant
{
    using UnityEngine;
    using System.Collections;

    public class WaterRockRemoveHandle : Handle
    {
        public WaterMission mission;
        void Start()
        {
        }

        public override void handle(Vector3 point)
        {
            base.handle(point);
            this.HideMe();
            this.RemoveRock();
        }

        private void RemoveRock()
        {
            this.mission.DestroyRock();
        }

        void HideMe()
        {
            GameObject.Destroy(gameObject);
        }
    }
}
