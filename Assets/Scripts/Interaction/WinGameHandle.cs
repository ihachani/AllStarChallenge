﻿using System;

using UnityEngine;
using System.Collections;
using System.Runtime.CompilerServices;

using theplant;

public class WinGameHandle : Handle
{
    public String Factory;
     
    public Rise Rise;

    public Tree Tree;

    void Start()
    {
    }

    public override void handle(Vector3 point)
    {
        base.handle(point);
        this.WinGame();
        this.IntroduceWillMillAt(point);
        this.DestroyFactory();
    }

    void DestroyFactory()
    {
        (GameObject.Find(this.Factory) as GameObject).GetComponent<Factory>().Clean();
    }

    void IntroduceWillMillAt(Vector3 point)
    {
        this.Rise.WindMillAt(point);
    }
    void WinGame()
    {
        this.Tree.createWindMill();
    }
}
