﻿using UnityEngine;
using System.Collections;
namespace theplant
{
    public class WaterMission : MonoBehaviour
    {
        public int RocksToDestroy = 2;
        public int rocksDestroyed = 0;

        public void DestroyRock()
        {
            rocksDestroyed++;
        }

        public bool IsAccomplished()
        {
            return rocksDestroyed == RocksToDestroy;
        }
    }
}
