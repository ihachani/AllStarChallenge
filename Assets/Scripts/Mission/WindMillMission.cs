﻿namespace theplant
{
    using UnityEngine;
    using System.Collections;

    public class WindMillMission : MonoBehaviour
    {
        public WaterMission previousMission;
        public int PartsToCollect = 3;
        public int PartsCollected = 0;

        public bool CanStartMission()
        {
            return this.previousMission.IsAccomplished();
        }

        public void CollectNewPart()
        {
            PartsCollected++;
        }

        public bool IsAccomplished()
        {
            return PartsCollected == PartsToCollect;
        }
    }
}
