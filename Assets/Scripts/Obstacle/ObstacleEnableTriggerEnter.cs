﻿namespace theplant
{
    using UnityEngine;
    using System.Collections;

    public class ObstacleEnableTriggerEnter : MonoBehaviour
    {
        [SerializeField]
        private GameObject obstacle;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                this.obstacle.SetActive(true);
            }
        }
    }

}