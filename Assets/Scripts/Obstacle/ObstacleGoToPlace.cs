﻿namespace theplant
{
    using System;

    using UnityEngine;

    [Serializable]
    public class ObstacleGoToPlace
    {
        [SerializeField]
        private float speed = 0.5f ;
        [SerializeField]
        private Vector3 destination;

        public Vector3 MoveToDestination(Vector3 start)
        {
            return Vector3.Lerp(start, this.destination, this.speed * Time.deltaTime);
        }

        public bool DestinationReached(Vector3 currentPosition)
        {
            return this.destination == currentPosition;
        }
        public Vector3 Destination
        {
            get
            {
                return this.destination;
            }
            set
            {
                this.destination = value;
            }
        }


        public float Speed
        {
            get
            {
                return this.speed;
            }
            set
            {
                this.speed = value;
            }
        }

    }
}