﻿namespace theplant
{
    using UnityEngine;
    using System.Collections;


    public class ObstacleGoToPlaceBehaviour : MonoBehaviour
    {
        [SerializeField]
        private ObstacleGoToPlace GoToPlace;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void GoToLocation()
        {

            //this.transform.position = this.GoToPlace.MoveToDestination(this.transform.position);
            StartCoroutine(GoToLocationCoroutine());
        }

        IEnumerator GoToLocationCoroutine()
        {
            while (!this.GoToPlace.DestinationReached(this.transform.position))
            {
                this.transform.position = this.GoToPlace.MoveToDestination(
                    this.transform.position);
                yield return null;
            }

        }
    }

}