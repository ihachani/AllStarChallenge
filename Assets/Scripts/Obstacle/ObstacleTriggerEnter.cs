﻿namespace theplant
{
    using UnityEngine;
    using System.Collections;

    public class ObstacleTriggerEnter : MonoBehaviour
    {
        [SerializeField]
        private ObstacleGoToPlaceBehaviour obstacleGoToPlaceBehaviour;

        void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                this.obstacleGoToPlaceBehaviour.GoToLocation();
            }
        }
    }

}