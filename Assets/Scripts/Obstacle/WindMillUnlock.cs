﻿using UnityEngine;
using System.Collections;

using theplant;

public class WindMillUnlock : MonoBehaviour
{
    [SerializeField]
    private float delay;
    [SerializeField]
    private ObstacleGoToPlaceBehaviour obstacleGoToPlaceBehaviour;

    public WindMillMission currentMission;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (this.currentMission.CanStartMission() && other.tag == "Player")
        {
            this.obstacleGoToPlaceBehaviour.GoToLocation();
            StartCoroutine(this.DestroyAfterDelay());
        }
    }

    IEnumerator DestroyAfterDelay()
    {
        float currentTime = 0f;
        while (currentTime < this.delay)
        {
            currentTime += Time.deltaTime;
            yield return null;
        }
        Destroy(this.gameObject);
    }
}
