﻿namespace theplant
{
    using System;

    using UnityEngine;
    using System.Collections;

    public class Rise : MonoBehaviour
    {
        [SerializeField]
        private Transform transform;

        public WindMillMission millMission;

        public String StoreGameObjectName = "Store";

        public GameObject WindMill;

        public void WindMillAt(Vector3 point)
        {
            if (this.millMission.IsAccomplished())
            {
                Instantiate(WindMill, this.transform.position, this.transform.rotation);
            }
        }
    }

}