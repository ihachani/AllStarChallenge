﻿namespace theplant
{
    using UnityEngine;
    using System.Collections;
    using System;
    using System.Collections.Generic;

    public class Store : MonoBehaviour
    {

        public Dictionary<string, int> elements = new Dictionary<string, int>();
        public String[] KindsOfItems;

        void Start()
        {
            foreach (String each in KindsOfItems)
            {
                elements.Add(each, 0);
            }
        }

        public bool HasWindMill()
        {
            foreach (String each in KindsOfItems)
            {
                if (elements[each] == 0) return false;
            }
            return true;
        }
        internal void AppendOne(string kind)
        {
            elements[kind] = elements[kind] + 1;
            Debug.Log("Store have" + elements[kind]);
        }
    }

}