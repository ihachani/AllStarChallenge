﻿namespace theplant
{
    using UnityEngine;
    using System.Collections;

    public class GoToLocationTest : MonoBehaviour
    {
        [SerializeField]
        private ObstacleGoToPlaceBehaviour ObstacleGoToPlaceBehaviour;
        // Use this for initialization
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            ObstacleGoToPlaceBehaviour.GoToLocation();
        }
    }

}