﻿namespace theplant
{
    using UnityEngine;
    using System.Collections;

    public class StartFadingScriptIntegrationTest : MonoBehaviour
    {
        [SerializeField]
        private FadeAfterDuration fadeAfterDuration;
        // Use this for initialization
        void Start()
        {
            this.fadeAfterDuration.Action();

        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}