﻿using System;

using UnityEngine;
using System.Collections;

using theplant;

public class Tree : MonoBehaviour
{

    public WaterMission AllowWater;
    public WindMillMission CollectWindMill;

    public GameObject[] doodles;
    [SerializeField]
    private Transform[] doodlesTransforms;
    public int Level = 1;

    public GameObject SmallTree;
    public GameObject MediumTree;
    public GameObject BigTree;

    void Start()
    {
    }

    public void createWindMill()
    {
        for (int i = 0; i < this.doodles.Length; i++)
        {
            Instantiate(doodles[i], this.doodlesTransforms[i].position,
                this.doodlesTransforms[i].rotation);
        }
    }
    void Update()
    {
        if (this.CollectWindMill.IsAccomplished() && this.Level == 2)
        {
            this.Level = 3;
            this.MediumTree.SetActive(false);
            this.BigTree.SetActive(true);

        }
        else
        {
            if (this.AllowWater.IsAccomplished() && this.Level == 1)
            {
                this.Level = 2;
                this.SmallTree.SetActive(false);
                this.MediumTree.SetActive(true);
            }
        }
    }

}
